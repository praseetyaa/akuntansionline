<?php
/**
 * Template Name: Home Page
 * 
 * @package WordPress
 * @subpackage Spandiv
 * @since Spandiv 1.0
 */
?>

<?php get_header();?>

<div id="spandiv">
	<section class="section-header py-5">
		<img class="img-overlay" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"alt="bg-image">
		<div class="mask py-lg-5">
			<div class="d-flex justify-content-end align-items-center h-100">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 order-2 order-lg-1" data-aos="fade">
							<h1 class="mb-4">Software Akuntansi Online<br>Terlengkap Di Indonesia</h1>
							<div class="d-block d-md-flex align-items-center mb-2">
								<div class="me-md-2 me-0 h5 fw-normal">
									<i class="fas fa-check-square"></i>
									<span>Realtime</span>
								</div>
								<div class="me-md-2 me-0 h5 fw-normal">
									<i class="fas fa-check-square"></i>
									<span>Multi Cabang</span>
								</div>
							</div>
							<div class="d-block d-md-flex align-items-center mb-4">
								<div class="me-md-2 me-0 h5 fw-normal">
									<i class="fas fa-check-square"></i>
									<span>Multi Devices</span>
								</div>
								<div class="me-md-2 me-0 h5 fw-normal">
									<i class="fas fa-check-square"></i>
									<span>Analysis Report</span>
								</div>
							</div>
							<a class="btn btn-lg btn-primary text-uppercase" href="https://member.akuntansionline.net/index.php?r=user/registration/step0"><i class="fas fa-gem"  style="animation: opacito 1s infinite ease;" rel="noreferrer"></i> Coba Gratis 15 Hari</a>
						</div>
						<div class="col-lg-6 mb-4 mb-lg-0 order-1 order-lg-2" data-aos="fade-left">
							<img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/banner-min.png" alt="banner-icon-akuntansi-online">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-benefit">
		<div class="container">
			<div class="card py-4 rounded-3 shadow" data-aos="fade-up">
				<div class="card-body">
					<div class="row">
						<div class="col-md-4 mb-3 mb-md-0 text-center">
							<div class="p-3 rounded-3 mb-2 mx-auto" style="background-color: var(--primary-s); width: fit-content;">
								<img width="60" src="<?php echo get_template_directory_uri() ?>/assets/images/icons/easy.png" 
								alt="icon" class="filter-primary">
							</div>
							<p class="fw-bold mb-0">Mudah</p>
							<p class="m-0">Akuntansi Online sangat mudah digunakan, dalam pencatatan laporan keuangan maupun melihat laporan keuangan</p>
						</div>
						<div class="col-md-4 mb-3 mb-md-0 text-center">
							<div class="p-3 rounded-3 mb-2 mx-auto" style="background-color: var(--primary-s); width: fit-content;">
								<img width="60" src="<?php echo get_template_directory_uri() ?>/assets/images/icons/analytics.png" 
								alt="icon" class="filter-primary">
							</div>
							<p class="fw-bold mb-0">Laporan Keuangan Lengkap</p>
							<p class="m-0">Akuntansi Online menampilkan laporan keuangan sangat lengkap, mulai dari Laba Rugi, Neraca, Arus Kas, sampai Rosetta Stone</p>
						</div>
						<div class="col-md-4 mb-3 mb-md-0 text-center">
							<div class="p-3 rounded-3 mb-2 mx-auto" style="background-color: var(--primary-s); width: fit-content;">
								<img width="60" src="<?php echo get_template_directory_uri() ?>/assets/images/icons/user.png" 
								alt="icon" class="filter-primary">
							</div>
							<p class="fw-bold mb-0">Multi Cabang</p>
							<p class="m-0">Akuntansi Online bisa digunakan untuk perusahaan anda yang memiliki banyak cabang, dan bisa menjadi Laporan Konsolidasi</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-service py-5">
		<div class="container">
			<div class="heading text-center mb-5 pb-5">
				<h1>Keuntungan Menggunakan<br>Akuntansi Online Untuk Bisnis Anda</h1>
			</div>
			<div class="row">
				<div class="col-md-4 mb-3 text-center" data-aos="fade">
					<img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon04-1.png" alt="icon" class="mb-2 filter-primary">
					<p class="fw-bold mb-0">Membuat Anggaran</p>
					<p>Di Akuntansi Online anda bisa membuat anggaran untuk biaya-biaya yang akan di keluarkan, jadi anda bisa mengetahui apakah semua sesuai rencana</p>
				</div>
				<div class="col-md-4 mb-3 text-center" data-aos="fade">
					<img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon07.png" alt="icon" class="mb-2 filter-primary">
					<p class="fw-bold mb-0">Laporan Keuangan</p>
					<p>Anda bisa memonitoring laporan keuangan laba rugi, neraca, rosetta stone dll, kapan pun dan di mana pun</p>
				</div>
				<div class="col-md-4 mb-3 text-center" data-aos="fade">
					<img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon09.png" alt="icon" class="mb-2 filter-primary">
					<p class="fw-bold mb-0">Berbasis Cloud</p>
					<p>Akuntansi Online menggunakan penyimpanan berbasis Cloud, jadi anda bisa dengan mudah mengakses laporan keuangan di mana pun dan kapan pun</p>
				</div>
				<div class="col-md-4 mb-3 text-center" data-aos="fade">
					<img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon05.png" alt="icon" class="mb-2 filter-primary">
					<p class="fw-bold mb-0">Pencatatan Biaya</p>
					<p>Melihat dan mengatur biaya pengeluaran dengan mudah</p>
				</div>
				<div class="col-md-4 mb-3 text-center" data-aos="fade">
					<img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon06.png" alt="icon" class="mb-2 filter-primary">
					<p class="fw-bold mb-0">Zero Maintanance</p>
					<p>Anda tidak perlu untuk maintanance software atau server, karena yang anda butuhkan hanya devices yang connect internet</p>
				</div>
				<div class="col-md-4 mb-3 text-center" data-aos="fade">
					<img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon08.png" alt="icon" class="mb-2 filter-primary">
					<p class="fw-bold mb-0">Multi Cabang</p>
					<p>Akuntansi Online memudahkan anda untuk mengelola keuangan banyak cabang dalam satu software</p>
				</div>
			</div>
		</div>
	</section>
	<section class="section-client py-5">
		<div class="container">
			<div class="heading text-center mb-5 pb-5">
				<h1>Harga<br>Akuntansi Online</h1>
			</div>
			<div class="row">
				<div class="col-lg-4 mb-4 mb-lg-0">
					<div class="card" data-aos="fade-up" data-aos-duration="200">
						<div class="card-header rounded-2 py-5 text-center border-0">
							<h4>Start UP</h4>
							<h1>Rp. 129.000</h1>
							<p class="m-0">Per Bulan</p>
						</div>
						<div class="card-body">
							<p class="mb-1"><i class="fas fa-check"></i> Catatan Penjualan & Pembelian</p>
							<p class="mb-1"><i class="fas fa-check"></i> Monitor Biaya Pengeluaran</p>
							<p class="mb-1"><i class="fas fa-check"></i> Kas/Bank : 2</p>
							<p class="mb-1"><i class="fas fa-check"></i> Max. User Login : 3</p>
							<p class="mb-1"><i class="fas fa-check"></i> Free User Login : 1</p>
							<p class="mb-1"><i class="fas fa-check"></i> Support Chat & Email</p>
							<p class="mb-1"><i class="fas fa-check"></i> Mobile Dashboard</p>
							<p class="mb-1"><i class="fas fa-check"></i> Daftar Akun Fleksibel & Buku Besar</p>
							<p class="mb-1"><i class="fas fa-check"></i> Laporan Bisnis & Keuangan</p>
							<p class="mb-1"><i class="fas fa-check"></i> Aksesibilitas, Keamanan Terjamin</p>
							<p class="mb-1"><i class="fas fa-times"></i> Cabang</p>
							<p class="mb-1"><i class="fas fa-times"></i> Pengaturan Aset Tetap Otomatis</p>
							<p class="mb-1"><i class="fas fa-times"></i> Anggaran Keuangan / Budgeting</p>
							<p class="mb-1"><i class="fas fa-times"></i> Free On-site Training</p>
							<p class="mb-1"><i class="fas fa-times"></i> Customize Feature</p>
							<div class="text-center">
								<a class="btn btn-lg btn-primary mt-4 mb-3" href="https://wa.me/<?php echo do_shortcode('[spandiv key="telephone"]') ?>?text=mohon%20info%20akuntansi%20online%20paket%20startup" target="_blank" rel="noreferrer">Pesan Sekarang</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mb-4 mb-lg-0">
					<div class="card" data-aos="fade-up" data-aos-duration="400">
						<div class="card-header rounded-2 py-5 text-center border-0">
							<h4>Professional</h4>
							<h1>Rp. 219.000</h1>
							<p class="m-0">Per Bulan</p>
						</div>
						<div class="card-body">
							<p class="mb-1"><i class="fas fa-check"></i> Catatan Penjualan & Pembelian</p>
							<p class="mb-1"><i class="fas fa-check"></i> Monitor Biaya Pengeluaran</p>
							<p class="mb-1"><i class="fas fa-check"></i> Kas/Bank : 5</p>
							<p class="mb-1"><i class="fas fa-check"></i> Max. User Login : 5</p>
							<p class="mb-1"><i class="fas fa-check"></i> Free User Login : 3</p>
							<p class="mb-1"><i class="fas fa-check"></i> Support Chat, Email & Call</p>
							<p class="mb-1"><i class="fas fa-check"></i> Mobile Dashboard</p>
							<p class="mb-1"><i class="fas fa-check"></i> Daftar Akun Fleksibel & Buku Besar</p>
							<p class="mb-1"><i class="fas fa-check"></i> Laporan Bisnis & Keuangan</p>
							<p class="mb-1"><i class="fas fa-check"></i> Aksesibilitas, Keamanan Terjamin</p>
							<p class="mb-1"><i class="fas fa-check"></i> Cabang : 3</p>
							<p class="mb-1"><i class="fas fa-check"></i> Pengaturan Aset Tetap Otomatis</p>
							<p class="mb-1"><i class="fas fa-check"></i> Anggaran Keuangan / Budgeting</p>
							<p class="mb-1"><i class="fas fa-times"></i> Free On-site Training</p>
							<p class="mb-1"><i class="fas fa-times"></i> Customize Feature</p>
							<div class="text-center">
								<a class="btn btn-lg btn-primary mt-4 mb-3" href="https://wa.me/<?php echo do_shortcode('[spandiv key="telephone"]') ?>?text=mohon%20info%20akuntansi%20online%20paket%20profesional" target="_blank" rel="noreferrer">Pesan Sekarang</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mb-4 mb-lg-0">
					<div class="card" data-aos="fade-up" data-aos-duration="600" style="border: 2px solid var(--primary); border-radius: 1em 1em .25em .25em;">
						<div class="card-header rounded-2 py-5 text-center border-0" style="color: var(--primary);background-color: var(--primary-s);">
							<div class="d-flex align-items-center justify-content-center"><h4 class="me-2">Business</h4><span class="badge" style="color: var(--primary); background-color: var(--bs-white)">Most Popular</span></div>
							<h1>Rp. 359.000</h1>
							<p class="m-0">Per Bulan</p>
						</div>
						<div class="card-body">
							<p class="mb-1"><i class="fas fa-check"></i> Catatan Penjualan & Pembelian</p>
							<p class="mb-1"><i class="fas fa-check"></i> Monitor Biaya Pengeluaran</p>
							<p class="mb-1"><i class="fas fa-check"></i> Kas/Bank : Unlimited</p>
							<p class="mb-1"><i class="fas fa-check"></i> Max. User Login : Unlimited</p>
							<p class="mb-1"><i class="fas fa-check"></i> Free User Login : 5</p>
							<p class="mb-1"><i class="fas fa-check"></i> Support Chat, Email, & Call</p>
							<p class="mb-1"><i class="fas fa-check"></i> Mobile Dashboard</p>
							<p class="mb-1"><i class="fas fa-check"></i> Daftar Akun Fleksibel & Buku Besar</p>
							<p class="mb-1"><i class="fas fa-check"></i> Laporan Bisnis & Keuangan</p>
							<p class="mb-1"><i class="fas fa-check"></i> Aksesibilitas, Keamanan Terjamin</p>
							<p class="mb-1"><i class="fas fa-check"></i> Cabang : 5</p>
							<p class="mb-1"><i class="fas fa-check"></i> Pengaturan Aset Tetap Otomatis</p>
							<p class="mb-1"><i class="fas fa-check"></i> Anggaran Keuangan / Budgeting</p>
							<p class="mb-1"><i class="fas fa-check"></i> Free On-site Training</p>
							<p class="mb-1"><i class="fas fa-check"></i> Customize Feature</p>
							<div class="text-center">
								<a class="btn btn-lg btn-primary mt-4 mb-3" href="https://wa.me/<?php echo do_shortcode('[spandiv key="telephone"]') ?>?text=mohon%20info%20akuntansi%20online%20paket%20business" target="_blank" rel="noreferrer">Pesan Sekarang</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div data-aos="fade-up" data-aos-duration="200">
		<div class="custom-shape-divider-bottom-1623205536">
		  <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
		      <path d="M0,0V7.23C0,65.52,268.63,112.77,600,112.77S1200,65.52,1200,7.23V0Z" class="shape-fill"></path>
		  </svg>
		</div>
		<section class="section-cta pb-5" style="background-color: var(--primary-s);">
			<div class="container text-center" data-aos="fade" data-aos-duration="600">
				<h1 style="color: var(--primary);">Jalankan Bisnis Anda lebih Mudah dan Cepat<br>dengan Akuntansi Online</h1>
				<div class="btn-container">
					<a class="btn btn-lg btn-primary button at" href="https://member.akuntansionline.net/index.php?r=user/registration/step0" ><i class="fas fa-gem"  style="animation: opacito 1s infinite ease;" rel="noreferrer"></i> Coba Gratis Sekarang</a>
				</div>
			</div>
		</section>
	</div>
	<style>
	.custom-shape-divider-top-1623133865{background-color: var(--primary-s); display: block;}
	</style>
</div>

<?php get_footer();?>