<?php
/**
 * Template Name: Halaman Harga
 * 
 * @package WordPress
 * @subpackage Spandiv
 * @since Spandiv 1.0
 */
get_header(); ?>

<?php get_template_part('template-parts/hero'); ?>

<section class="section-content py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mb-4 mb-lg-0">
                <div class="card">
                    <div class="card-header rounded-2 py-5 text-center border-0">
                        <h4>Start UP</h4>
                        <h1>Rp. 129.000</h1>
                        <p class="m-0">Per Bulan</p>
                    </div>
                    <div class="card-body">
                        <p class="mb-1"><i class="fas fa-check"></i> Catatan Penjualan & Pembelian</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Monitor Biaya Pengeluaran</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Kas/Bank : 2</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Max. User Login : 3</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Free User Login : 1</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Support Chat & Email</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Mobile Dashboard</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Daftar Akun Fleksibel & Buku Besar</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Laporan Bisnis & Keuangan</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Aksesibilitas, Keamanan Terjamin</p>
                        <p class="mb-1"><i class="fas fa-times"></i> Cabang</p>
                        <p class="mb-1"><i class="fas fa-times"></i> Pengaturan Aset Tetap Otomatis</p>
                        <p class="mb-1"><i class="fas fa-times"></i> Anggaran Keuangan / Budgeting</p>
                        <p class="mb-1"><i class="fas fa-times"></i> Free On-site Training</p>
                        <p class="mb-1"><i class="fas fa-times"></i> Customize Feature</p>
                        <div class="text-center">
                            <a class="btn btn-lg btn-primary mt-4 mb-3" href="https://wa.me/<?php echo do_shortcode('[spandiv key="telephone"]') ?>?text=mohon%20info%20akuntansi%20online%20paket%20startup" target="_blank" rel="noreferrer">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4 mb-lg-0">
                <div class="card">
                    <div class="card-header rounded-2 py-5 text-center border-0">
                        <h4>Professional</h4>
                        <h1>Rp. 219.000</h1>
                        <p class="m-0">Per Bulan</p>
                    </div>
                    <div class="card-body">
                        <p class="mb-1"><i class="fas fa-check"></i> Catatan Penjualan & Pembelian</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Monitor Biaya Pengeluaran</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Kas/Bank : 5</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Max. User Login : 5</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Free User Login : 3</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Support Chat, Email & Call</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Mobile Dashboard</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Daftar Akun Fleksibel & Buku Besar</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Laporan Bisnis & Keuangan</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Aksesibilitas, Keamanan Terjamin</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Cabang : 3</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Pengaturan Aset Tetap Otomatis</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Anggaran Keuangan / Budgeting</p>
                        <p class="mb-1"><i class="fas fa-times"></i> Free On-site Training</p>
                        <p class="mb-1"><i class="fas fa-times"></i> Customize Feature</p>
                        <div class="text-center">
                            <a class="btn btn-lg btn-primary mt-4 mb-3" href="https://wa.me/<?php echo do_shortcode('[spandiv key="telephone"]') ?>?text=mohon%20info%20akuntansi%20online%20paket%20profesional" target="_blank" rel="noreferrer">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4 mb-lg-0">
                <div class="card" style="border: 2px solid var(--primary); border-radius: 1em 1em .25em .25em;">
                    <div class="card-header rounded-2 py-5 text-center border-0" style="color: var(--primary);background-color: var(--primary-s);">
                        <div class="d-flex align-items-center justify-content-center"><h4 class="me-2">Business</h4><span class="badge" style="color: var(--primary); background-color: var(--bs-white)">Most Popular</span></div>
                        <h1>Rp. 359.000</h1>
                        <p class="m-0">Per Bulan</p>
                    </div>
                    <div class="card-body">
                        <p class="mb-1"><i class="fas fa-check"></i> Catatan Penjualan & Pembelian</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Monitor Biaya Pengeluaran</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Kas/Bank : Unlimited</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Max. User Login : Unlimited</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Free User Login : 5</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Support Chat, Email, & Call</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Mobile Dashboard</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Daftar Akun Fleksibel & Buku Besar</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Laporan Bisnis & Keuangan</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Aksesibilitas, Keamanan Terjamin</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Cabang : 5</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Pengaturan Aset Tetap Otomatis</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Anggaran Keuangan / Budgeting</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Free On-site Training</p>
                        <p class="mb-1"><i class="fas fa-check"></i> Customize Feature</p>
                        <div class="text-center">
                            <a class="btn btn-lg btn-primary mt-4 mb-3" href="https://wa.me/<?php echo do_shortcode('[spandiv key="telephone"]') ?>?text=mohon%20info%20akuntansi%20online%20paket%20business" target="_blank" rel="noreferrer">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div>
    <div class="custom-shape-divider-bottom-1623205536">
      <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
          <path d="M0,0V7.23C0,65.52,268.63,112.77,600,112.77S1200,65.52,1200,7.23V0Z" class="shape-fill"></path>
      </svg>
    </div>
    <section class="section-cta pb-5" style="background-color: var(--primary-s);">
        <div class="container text-center">
            <h1 style="color: var(--primary);">Jalankan Bisnis Anda lebih Mudah dan Cepat<br>dengan Akuntansi Online</h1>
            <div class="btn-container">
                <a class="btn btn-lg btn-primary button at" href="https://member.akuntansionline.net/index.php?r=user/registration/step0" ><i class="fas fa-gem"  style="animation: opacito 1s infinite ease;" rel="noreferrer"></i> Coba Gratis Sekarang</a>
            </div>
        </div>
    </section>
</div>
<style>
.custom-shape-divider-top-1623133865{background-color: var(--primary-s); display: block;}
</style>
<?php get_footer(); ?>