<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Spandiv
 * @since Spandiv 1.0
 */

if (!function_exists('spandiv_setup')) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @since Faturmedia 1.0
     *
     * @return void
     */
    function spandiv_setup() {
        // Add theme support for some HTML5
        add_theme_support('html5', [
            'comment-form', 'comment-list'
        ]);

        // Add theme support for post thumbnails
        add_theme_support('post-thumbnails');
    }
}

add_action('after_setup_theme', 'spandiv_setup');

// Search post only
function search_post_only($query) {
    if($query->is_search) {
        $query->set('post_type', 'post');
    }
    return $query;
}
add_filter('pre_get_posts', 'search_post_only');

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo home_url() ?>/wp-content/uploads/2019/10/LOGO-AKUNTANSI-ONLINE-100x47.png);
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// widget
if ( function_exists('register_sidebar') )
// posts ads
register_sidebar(array(
    'name' => 'sidebar-widget',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
    )
);