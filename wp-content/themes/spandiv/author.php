<?php
/**
 * Template Name: Author Page
 * 
 * @package WordPress
 * @subpackage Spandiv
 * @since Spandiv 1.0
 */
?>
 
<?php get_header(); ?> 

<?php get_template_part('template-parts/hero'); ?>

<!-- Content -->
<section class="section-content py-5">
    <div class="container">
        <div class="row">
            <!-- Posts -->
            <div class="col-lg-9 mb-4 mb-lg-0">
                <?php get_template_part('template-parts/posts'); ?>
            </div>

            <!-- Sidebar -->
            <div class="col-lg-3">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>   
</section>

<?php get_footer(); ?>