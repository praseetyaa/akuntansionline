<?php
/**
 * Template Name: Single Post
 * 
 * @package WordPress
 * @subpackage Spandiv
 * @since Spandiv 1.0
 */
?>

<?php get_header(); ?>

<?php if(!is_front_page()) : get_template_part('template-parts/hero'); endif; ?>

<?php if(have_posts()) : ?>

<!-- Content -->
<section class="section-content py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 mb-4 mb-lg-0">

                <?php while(have_posts()) : the_post(); ?>

                    <div class="post-detail">
                        <!-- author -->
                        <div class="d-flex">
                            <p class="me-3 text-capitalize"><i class="far fa-user"></i> <a href="<?= get_author_posts_url($post->post_author) ?>"><?php the_author() ?></a></p>
                            <p class="me-3"><i class="far fa-calendar"></i> <?php the_date() ?></p>
                            <p><i class="far fa-clock"></i> <?php the_time() ?></p>
                        </div>          

                        <?php if(has_post_thumbnail()) : ?>
                            <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" class="rounded-1 single-thumbnail" style="background-image: url('<?= the_post_thumbnail_url() ?>');">
                        <?php else : ?>
                            <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" class="rounded-1 single-thumbnail" style="background-image: url('<?= bloginfo('template_url') ?>/assets/images/placeholder_600x400.svg');">
                        <?php endif; ?>
                        <div class="mt-3"><?= the_content() ?></div>

                        <?php $post_tags = get_the_terms(get_the_ID(), 'post_tag'); if($post_tags) : ?>
                            <hr>
                            <div>
                                <?php foreach($post_tags as $tag): ?>
                                <a href="<?= get_tag_link($tag) ?>"><span class="badge bg-primary">#<?= $tag->name ?></span></a>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>

                <?php endwhile; ?>

                <hr>
                
                <?php
                    // If comments are open or there is at least one comment, load up the comment template.
                    if(comments_open() || get_comments_number()) comments_template();
                ?>

            </div>

            <!-- Sidebar -->
            <div class="col-lg-3">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>

<?php else : ?>

<?php endif; ?>

<?php get_footer(); ?>