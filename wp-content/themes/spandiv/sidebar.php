<div class="sticky-top" style="top: 5rem">
	<div class="login_author text-center mb-3 rounded" style="background-color: var(--primary-s);">
		<a class="text-decoration-none author-login" href="<?php echo home_url() ?>/wp-admin/">
			<div class="p-4">
				<p class="mb-0 h1"><i class="far fa-user-astronaut"></i></p>
				<?php $current_user = wp_get_current_user();
				if ($current_user->display_name != null) { ?>
					<p class="mb-0">Selamat Datang</p>
					<h5><?php echo $current_user->display_name; ?></h5>
				<?php } else { ?>
						<h5>Author?<br>Login Disini</h5>
				<?php } ?> 
			</div>
		</a>
	</div>
	<!-- widget -->
	<div class="widget-posts-ads sticky-md-top" style="top: 6rem; z-index:1">
	    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar-widget'") ) : ?>
	    <?php endif;?>
	</div>
</div>
<style>
	.author-login{color: var(--primary);}
	.author-login:hover i{animation: rotate45 .5s infinite;}
	@keyframes rotate45 {
		0%{transform: rotate(0deg);}
		50%{transform: rotate(45deg);}
		100%{transform: rotate(0deg);}
	}
</style>