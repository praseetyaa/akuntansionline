<?php
/**
 * Template Name: Halaman Kontak
 * 
 * @package WordPress
 * @subpackage Spandiv
 * @since Spandiv 1.0
 */
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/hero'); ?>

<section class="section-benefit pt-5 pt-md-0">
    <div class="container">
        <div class="card p-3 rounded-3 shadow-sm">
            <div class="card-body d-block d-md-flex">
                <div class="text-center txet-md-start">
                <img width="80" class="me-3 mb-3 mb-md-0 filter-primary" src="<?php echo get_template_directory_uri() ?>/assets/images/icons/customer-service.png" alt="customer-service"></div>
                <div>
                    <h5>Diskusikan kebutuhan bisnis Anda dengan kami</h5>
                    <p class="mb-0">Kami ingin menunjukkan bagaimana Akuntansi Online dapat memecahkan masalah yang Anda hadapi dalam bisnis Anda. Jelaskan permasalahan bisnis Anda dengan kami, agar kami dapat memberikan solusi yang paling tepat untuk bisnis Anda</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-content">
    <div class="container py-5">
        <div class="row">
            <div class="col-md-6">
                <div class="card py-4">
                    <div class="card-img-top text-center">
                        <div class="bg-primary-s w-fit mx-auto p-3 rounded-1"><img width="50" class="mb-md-0 filter-primary" src="<?php echo get_template_directory_uri() ?>/assets/images/icons/telephone.png" alt="customer-service"></div>
                        <h3 class="mt-3 text-primary">Telepon</h3>
                    </div>
                    <div class="card-body text-center">
                        <p>Bicara langsung dengan<br>konsultan kami melalui Telepon</p>
                        <a class="btn btn-primary" href="tel:(024) 3583732">Hubungi Sekarang</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card py-4">
                    <div class="card-img-top text-center">
                        <div class="bg-primary-s w-fit mx-auto p-3 rounded-1"><img width="50" class="mb-md-0 filter-primary" src="<?php echo get_template_directory_uri() ?>/assets/images/icons/whatsapp.png" alt="customer-service"></div>
                        <h3 class="mt-3 text-primary">WhatsApp</h3>
                    </div>
                    <div class="card-body text-center">
                        <p>Bicara langsung dengan<br>konsultan kami melalui WhatsApp</p>
                        <a class="btn btn-primary" href="https://wa.me/<?php echo do_shortcode('[spandiv key="telephone"]') ?>">Hubungi Sekarang</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-map">
    <div class="container py-5">
        <?php echo do_shortcode('[spandiv key="google_maps"]') ?>
    </div>
</section>
<div>
    <div class="custom-shape-divider-bottom-1623205536">
      <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
          <path d="M0,0V7.23C0,65.52,268.63,112.77,600,112.77S1200,65.52,1200,7.23V0Z" class="shape-fill"></path>
      </svg>
    </div>
    <section class="section-cta pb-5" style="background-color: var(--primary-s);">
        <div class="container text-center">
            <h1 style="color: var(--primary);">Jalankan Bisnis Anda lebih Mudah dan Cepat<br>dengan Akuntansi Online</h1>
            <div class="btn-container">
                <a class="btn btn-lg btn-primary button at" href="https://member.akuntansionline.net/index.php?r=user/registration/step0" ><i class="fas fa-gem"  style="animation: opacito 1s infinite ease;" rel="noreferrer"></i> Coba Gratis Sekarang</a>
            </div>
        </div>
    </section>
</div>
<style type="text/css">
.custom-shape-divider-top-1623133865{background-color: var(--primary-s); display: block;}
@media (max-width: 768px) {.section-benefit{top: -3rem;} }
@media (max-width: 425px) {.section-benefit{top: 0rem;} }
</style>
<script type="text/javascript">

function initialize() {

  var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

  // Resize stuff...
  google.maps.event.addDomListener(window, "resize", function() {
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center); 
  });

}

</script>
<?php get_footer();?>