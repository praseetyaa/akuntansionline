<div class="row">

    <?php if(have_posts()) : ?>

        <?php while(have_posts()) : the_post(); ?>

            <?php $post = $wp_query->post; // Catch post ?>

            <article class="col-md-6 col-lg-4 mb-3">
                <div class="card shadow-sm border-0 h-100">
                    <a href="<?= the_permalink() ?>">
                        <?php if(has_post_thumbnail()) : ?>
                            <img class="card-img-top" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="background-image: url('<?= the_post_thumbnail_url() ?>'); background-position: center; background-size: cover; height: 200px;">
                        <?php else : ?>
                            <img class="card-img-top" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="background-image: url('<?= bloginfo('template_url') ?>/assets/images/placeholder_600x400.svg'); background-position: center; background-size: cover; height: 200px;">
                        <?php endif; ?>
                    </a>
                    <div class="card-body">
                        <p class="fw-bold"><a class="link-dark text-decoration-none" href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
                        <p class="mb-0">
                            <?php $categories = get_the_category(); foreach($categories as $category) : ?>
                                <a href="<?= get_category_link($category->term_id) ?>"><span class="badge bg-primary"><?= $category->cat_name ?></span></a>
                            <?php endforeach; ?>
                        </p>
					</div>
					<div class="card-footer bg-transparent">
						<small class="text-muted me-2"><i class="far fa-calendar me-1"></i><?= date('d/m/Y', strtotime($post->post_date)) ?></small>
						<small class="text-muted"><i class="far fa-user me-1"></i><a class="link-primary text-decoration-none text-capitalize" href="<?= get_author_posts_url($post->post_author) ?>"><?= get_the_author() ?></a></small>
                    </div>
                </div>
            </article>

        <?php endwhile; ?>

	<?php the_posts_pagination(); ?>
	
    <?php else : ?>

        <div class="col-12 mb-3">
            <div class="alert alert-danger">Tidak ada.</div>
        </div>

    <?php endif; ?>

</div>