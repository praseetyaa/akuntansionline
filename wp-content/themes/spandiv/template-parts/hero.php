<section class="section-header section-blog py-5">
    <div class="container text-center py-0 py-lg-5"> 
        <div>
            <?php if(is_home() || is_page() || is_single()) : ?>
                <h1><?= single_post_title() ?></h1>
                <?php if (is_page('harga')) : ?>
                    <h5>Daftar Pilihan Harga Akuntansi Online</h5> 
                <?php elseif (is_page('fitur')) : ?>
                    <h5>Fitur Lengkap Akuntansi Online</h5>
                <?php elseif(is_home()) : ?>
                    <h5>Artikel, tips, dan checklist menarik yang disiapkan bagi bisnis Anda</h5>
                <?php endif; ?>                      
            <?php elseif(is_category()) : ?>
                <h1><?= single_cat_title() ?></h1>
            <?php elseif(is_tag()) : ?>
                <h1><?= single_tag_title() ?></h1>
            <?php elseif(is_author()) : ?>
                <h1 class="text-capitalize"><?= get_the_author() ?></h1>
            <?php elseif(is_search()) : ?>
                <h1><?= _e('Pencarian') ?></h1>
            <?php endif; ?>
        </div>
        
        <?php if(is_search()) : ?>
            <p>Menampilkan pencarian dengan kata kunci: <strong><?= get_search_query() ?></strong></p>
        <?php else : ?>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="<?= home_url() ?>">Beranda</a></li>
                    <?php if(is_single() || is_category() || is_tag() || is_author()) : ?>
                        <li class="breadcrumb-item"><a href="<?= home_url('blog') ?>">Blog</a></li>
                    <?php endif; ?>
                    <?php if(is_home() || is_page() || is_single()) : ?>
                        <li class="breadcrumb-item active" aria-current="page"><?= single_post_title() ?></li>
                    <?php elseif(is_category()) : ?>
                        <li class="breadcrumb-item active" aria-current="page"><?= single_cat_title() ?></li>
                    <?php elseif(is_tag()) : ?>
                        <li class="breadcrumb-item active" aria-current="page"><?= single_tag_title() ?></li>
                    <?php elseif(is_author()) : ?>
                        <li class="breadcrumb-item active text-capitalize" aria-current="page"><?= get_the_author() ?></li>
                    <?php endif; ?>
                </ol>
            </nav>
        <?php endif; ?>
        <?php if(is_home() || is_search() ) : ?>
            <form method="get" action="<?php echo home_url(); ?>" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" aria-label="Pencarian" aria-describedby="search_input" placeholder="<?php echo esc_attr_x( 'Pencarian Artikel', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
                    <button class="input-group-text" id="search_input" style="background-color: var(--primary-s);"><i class="far fa-search" style="color: var(--primary)"></i></button>
                </div>
            </form>
        <?php endif; ?>
    </div>
</section>