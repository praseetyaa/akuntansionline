<?php 
/**
 * Template Name: Halaman Fitur
 * 
 * @package WordPress
 * @subpackage Spandiv
 * @since Spandiv 1.0
 */
?>
<?php get_header(); ?>

<?php get_template_part('template-parts/hero'); ?>

<section class="section-benefit">
    <div class="container">
        <div class="card py-4 rounded-3 shadow-sm">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 mb-3 mb-md-0 text-center">
                        <div class="p-3 rounded-3 mb-2 mx-auto" style="background-color: var(--primary-s); width: fit-content;">
                            <img width="60" src="<?php echo get_template_directory_uri() ?>/assets/images/icons/easy.png" 
                            alt="icon" class="filter-primary">
                        </div>
                        <p class="fw-bold mb-0">Mudah</p>
                        <p class="m-0">Akuntansi Online sangat mudah digunakan, dalam pencatatan laporan keuangan maupun melihat laporan keuangan</p>
                    </div>
                    <div class="col-md-4 mb-3 mb-md-0 text-center">
                        <div class="p-3 rounded-3 mb-2 mx-auto" style="background-color: var(--primary-s); width: fit-content;">
                            <img width="60" src="<?php echo get_template_directory_uri() ?>/assets/images/icons/analytics.png" 
                            alt="icon" class="filter-primary">
                        </div>
                        <p class="fw-bold mb-0">Laporan Keuangan Lengkap</p>
                        <p class="m-0">Akuntansi Online menampilkan laporan keuangan sangat lengkap, mulai dari Laba Rugi, Neraca, Arus Kas, sampai Rosetta Stone</p>
                    </div>
                    <div class="col-md-4 mb-3 mb-md-0 text-center">
                        <div class="p-3 rounded-3 mb-2 mx-auto" style="background-color: var(--primary-s); width: fit-content;">
                            <img width="60" src="<?php echo get_template_directory_uri() ?>/assets/images/icons/user.png" 
                            alt="icon" class="filter-primary">
                        </div>
                        <p class="fw-bold mb-0">Multi Cabang</p>
                        <p class="m-0">Akuntansi Online bisa digunakan untuk perusahaan anda yang memiliki banyak cabang, dan bisa menjadi Laporan Konsolidasi</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-service py-5">
    <div class="container">
        <div class="heading text-center mb-5 pb-5">
            <h1>Keuntungan Menggunakan<br>Akuntansi Online Untuk Bisnis Anda</h1>
        </div>
        <div class="row">
            <div class="col-md-4 mb-3 text-center">
                <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon04-1.png" alt="icon" class="mb-2 filter-primary">
                <p class="fw-bold mb-0">Membuat Anggaran</p>
                <p>Di Akuntansi Online anda bisa membuat anggaran untuk biaya-biaya yang akan di keluarkan, jadi anda bisa mengetahui apakah semua sesuai rencana</p>
            </div>
            <div class="col-md-4 mb-3 text-center">
                <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon07.png" alt="icon" class="mb-2 filter-primary">
                <p class="fw-bold mb-0">Laporan Keuangan</p>
                <p>Anda bisa memonitoring laporan keuangan laba rugi, neraca, rosetta stone dll, kapan pun dan di mana pun</p>
            </div>
            <div class="col-md-4 mb-3 text-center">
                <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon09.png" alt="icon" class="mb-2 filter-primary">
                <p class="fw-bold mb-0">Berbasis Cloud</p>
                <p>Akuntansi Online menggunakan penyimpanan berbasis Cloud, jadi anda bisa dengan mudah mengakses laporan keuangan di mana pun dan kapan pun</p>
            </div>
            <div class="col-md-4 mb-3 text-center">
                <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon05.png" alt="icon" class="mb-2 filter-primary">
                <p class="fw-bold mb-0">Pencatatan Biaya</p>
                <p>Melihat dan mengatur biaya pengeluaran dengan mudah</p>
            </div>
            <div class="col-md-4 mb-3 text-center">
                <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon06.png" alt="icon" class="mb-2 filter-primary">
                <p class="fw-bold mb-0">Zero Maintanance</p>
                <p>Anda tidak perlu untuk maintanance software atau server, karena yang anda butuhkan hanya devices yang connect internet</p>
            </div>
            <div class="col-md-4 mb-3 text-center">
                <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/09/construction-icon08.png" alt="icon" class="mb-2 filter-primary">
                <p class="fw-bold mb-0">Multi Cabang</p>
                <p>Akuntansi Online memudahkan anda untuk mengelola keuangan banyak cabang dalam satu software</p>
            </div>
        </div>
    </div>
</section>
<section class="section-easy-start py-5">
    <div class="container">
        <div class="heading text-center mb-5 pb-5">
            <h1>Bagaimana Akuntansi Online Akan Memudahkan Anda Dalam Mengelola Bisnis</h1>
        </div>
        <div class="row">
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Realtime</p>
                        <p>Anda bisa mengakses Laporan Akuntansi Online Kapan pun</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Multi User</p>
                        <p>Tiap tiap bagian bisa mempunyai akses ke akuntansi online, dengan hak akses yang berbeda-beda</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Multi Cabang</p>
                        <p>Akuntansi Online dapat dengan otomatis menggabungkan laporan dari cabang / branch, untuk menjadi laporan Konsolidasi</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Multi Devices</p>
                        <p>Akuntansi Online dapat di akses di berbagai perangkat dan sistem operasi</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Flexible</p>
                        <p>Menu dan akun di akuntansi online bisa di custom sesuai kebutuhan bisnis anda</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Pencatatan Biaya</p>
                        <p>Melihat dan mengatur biaya pengeluaran dengan mudah</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Budgeting</p>
                        <p>Dengan Akuntansi Online, anda bisa membuat budget biaya bulanan bahkan tahunan untuk bisnis anda</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Target</p>
                        <p>Anda bisa membuat target penjualan bulanan bahkan tahunan</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Compare Report</p>
                        <p>Dengan Akuntansi Online, anda dapat membandingakan laporan keuangan anda dengan bulan sebelum nya bahkan dengan tahun sebelumnya</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Rosetta Stone</p>
                        <p>Anda bisa menganalisa bisnis dengan mudah, menggunakan fitur Rosetta Stone</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Zero Maintanance</p>
                        <p>Anda tidak perlu melakukan maintanance software atau server, karena yang anda butuhkan hanya devices yang connect internet</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Cloud</p>
                        <p>Semua laporan Akuntansi Online tersimpan aman di Cloud, dan anda bisa akes di mana pun</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Infographic Report</p>
                        <p>Report juga tersedia dalam infographic yang memudahkan anda dalam membaca laporan</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Export</p>
                        <p>Anda bisa meng-Export laporan anda menjadi file PDF atau CSV</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 b-3">
                <div class="d-flex">
                    <i class="far fa-check me-2 h5"></i>
                    <div>
                        <p class="fw-bold mb-0">Invoice</p>
                        <p>Anda bisa dengan mudah membuat Invoice dan langsung tehubung dengan laporan</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div>
    <div class="custom-shape-divider-bottom-1623205536">
      <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
          <path d="M0,0V7.23C0,65.52,268.63,112.77,600,112.77S1200,65.52,1200,7.23V0Z" class="shape-fill"></path>
      </svg>
    </div>
    <section class="section-cta pb-5" style="background-color: var(--primary-s);">
        <div class="container text-center">
            <h1 style="color: var(--primary);">Jalankan Bisnis Anda lebih Mudah dan Cepat<br>dengan Akuntansi Online</h1>
            <div class="btn-container">
                <a class="btn btn-lg btn-primary button at" href="https://member.akuntansionline.net/index.php?r=user/registration/step0" ><i class="fas fa-gem"  style="animation: opacito 1s infinite ease;" rel="noreferrer"></i> Coba Gratis Sekarang</a>
            </div>
        </div>
    </section>
</div>
<style>
.custom-shape-divider-top-1623133865{background-color: var(--primary-s); display: block;}
</style>
<?php get_footer(); ?>
