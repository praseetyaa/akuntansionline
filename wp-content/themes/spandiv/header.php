<!DOCTYPE html>
 <html <?php language_attributes(); ?>>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>"/>
	    <meta name="viewport" content="width=device-width, initial-scale=1" />
	    <meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />

	    <!-- Title -->
	    <?php if(is_front_page()) : ?>
	        <title><?= bloginfo('name') ?> - <?= bloginfo('description') ?></title>
	    <?php elseif(is_home() || is_page() || is_single()) : ?>
	        <title><?= single_post_title() ?> | <?= bloginfo('name') ?> - <?= bloginfo('description') ?></title>
	    <?php elseif(is_category()) : ?>
	        <title><?= single_cat_title() ?> | <?= bloginfo('name') ?> - <?= bloginfo('description') ?></title>
	    <?php elseif(is_tag()) : ?>
	        <title><?= single_tag_title() ?> | <?= bloginfo('name') ?> - <?= bloginfo('description') ?></title>
	    <?php elseif(is_author()) : ?>
	        <title><?= get_the_author() ?> | <?= bloginfo('name') ?> - <?= bloginfo('description') ?></title>
	    <?php elseif(is_search()) : ?>
	        <title><?= _e('Search') ?> | <?= bloginfo('name') ?> - <?= bloginfo('description') ?></title>
	    <?php elseif(is_404()) : ?>
	        <title>Error 404 | <?= bloginfo('name') ?> - <?= bloginfo('description') ?></title>
	    <?php endif; ?>

		<!-- font awesome -->
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.2.0/css/all.css">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/bootstrap/css/bootstrap.min.css" type="text/css" />
		<!-- fonts -->
		<link href="https://fonts.googleapis.com/css2?family=Satisfy&display=swap" rel="stylesheet">
		<!-- aos -->
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" media="screen" />
		
		<?php echo do_shortcode('[spandiv key="google_tag_manager"]') ?>
		
		<?php wp_head(); ?>
	</head>

	<body <?php body_class('spandiv'); ?>>

    <!-- WP Body -->
    <?php wp_body_open(); ?>
    
	<?php get_template_part('template-parts/navbar'); ?>
